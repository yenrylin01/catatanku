<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap demo</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
  </head>
  <body stayle="background:#86A3B8">
  <nav class="navbar container navbar-expand-lg bg-body-secondary">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">Notes</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="/home">Home</a>
        </li>
    
      </ul>
      @auth
      <h6 style="font-family: 'Inconsolata', monospace;">Welcome back,{{ auth()->user()->name }}</h6>
      
      <form action="/logout" method="post">
                  @csrf
                  <button type="submit" class="dropdown-item text-dark"><i class="bi bi-box-arrow-left"></i>   &nbsp;   <img src="https://img.icons8.com/color/28/null/exit.png"/>
</button> 
                </form>
      @else
      <h5></h5>@endauth
    </div>
  </div>
</nav>
<br><br>

    <div class="container">
        @yield('content')
    </div>

    <!--footer-->
    <hr>
    <div class="text-center">
        yerin-notes app 
    </div>

    
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
  </body>
</html>