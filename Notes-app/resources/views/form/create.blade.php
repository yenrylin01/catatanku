<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
<style>
    button {
 border: 2px solid #24b4fb;
 background-color: #24b4fb;
 border-radius: 0.9em;
 padding: 0.8em 1.2em 0.8em 1em;
 transition: all ease-in-out 0.2s;
 font-size: 16px;
}
body{
    background-color:#86A3B8 50%;
}

button span {
 display: flex;
 justify-content: center;
 align-items: center;
 color: #fff;
 font-weight: 600;
}

button:hover {
 background-color: #0071e2;
}
</style>

<div class="container contact-form">
            <div class="contact-image ">
                <img src="note.png" alt="rocket_contact" width="30%" class= "justify-content-center" />
            </div>
            <form method="post">
                @csrf
                <h3>Add a New Notes..</h3>
               <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <input type="text" name="judul" class="form-control" placeholder="Title" value="" />
                        </div>
                        <div class="form-group">
                            <input type="sifat" name="sifat" class="form-control" placeholder="Sifat" value="" />
                        </div>
                       
                        <button>
  <span>
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"></path><path fill="currentColor" d="M11 11V5h2v6h6v2h-6v6h-2v-6H5v-2z"></path></svg> Create
  </span>
</button>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <textarea name="deskripsi" class="form-control" placeholder="Your Description.." style="width: 100%; height: 150px;"></textarea>
                        </div>
                    </div>
                </div>
            </form>
</div>