@extends('layout.layout')
<br>
<!-- UIkit CSS -->
<link
    rel="stylesheet"
    href="https://cdn.jsdelivr.net/npm/uikit@3.15.22/dist/css/uikit.min.css"/>

<!-- UIkit JS -->
<script src="https://cdn.jsdelivr.net/npm/uikit@3.15.22/dist/js/uikit.min.js"></script>
<script
    src="https://cdn.jsdelivr.net/npm/uikit@3.15.22/dist/js/uikit-icons.min.js"></script>

@section('content')
<style>
    body{
        background-color:#86A3B8;
    }
    .cta {
        position: relative;
        margin: auto;
        padding: 12px 18px;
        transition: all 0.2s ease;
        border: none;
        background: none;
    }

    .cta:before {
        content: "";
        position: absolute;
        top: 0;
        left: 0;
        display: block;
        border-radius: 50px;
        background: #b1dae7;
        width: 45px;
        height: 45px;
        transition: all 0.3s ease;
    }

    .cta span {
        position: relative;
        font-family: "Ubuntu", sans-serif;
        font-size: 18px;
        font-weight: 700;
        letter-spacing: 0.05em;
        color: #234567;
    }

    .cta svg {
        position: relative;
        top: 0;
        margin-left: 10px;
        fill: none;
        stroke-linecap: round;
        stroke-linejoin: round;
        stroke: #234567;
        stroke-width: 2;
        transform: translateX(-5px);
        transition: all 0.3s ease;
    }

    .cta:hover:before {
        width: 100%;
        background: #b1dae7;
    }

    .cta:hover svg {
        transform: translateX(0);
    }

    .cta:active {
        transform: scale(0.95);
    }
    .scene {
        width: 10em;
        justify-content: center;
        align-items: center;
    }

    .cube {
        color: #ccc;
        cursor: pointer;
        font-family: 'Roboto', sans-serif;
        transition: all 0.85s cubic-bezier(.17,.67,.14,.93);
        transform-style: preserve-3d;
        transform-origin: 100% 50%;
        width: 10em;
        height: 4em;
    }

    .cube:hover {
        transform: rotateX(-90deg);
    }

    .side {
        box-sizing: border-box;
        position: absolute;
        display: inline-block;
        height: 4em;
        width: 10em;
        text-align: center;
        text-transform: uppercase;
        padding-top: 1.5em;
        font-weight: bold;
    }

    .top {
        background: wheat;
        color: #222229;
        transform: rotateX(90deg) translate3d(0, 0, 2em);
        box-shadow: inset 0 0 0 5px #fff;
    }

    .front {
        background: #F48484;
        color: #fff;
        box-shadow: inset 0 0 0 5px #fff;
        transform: translate3d(0, 0, 2em);
    }
</style>
<div class="conteiner">
    <div class="row">
        <div class="col-sm-4">
            <button class="cta">
                <span>
                    <a href="/create" style="color:black; text-decoration:none;">
                        Add new notes..</a>
                </span>
                <svg viewbox="0 0 13 10" height="10px" width="15px">
                    <path d="M1,5 L11,5"></path>
                    <polyline points="8 1 12 5 8 9"></polyline>
                </svg>
            </button>
        </div>
        <div class="col-sm-2 "></div>
        <div class="col-sm-2">
            <div class="scene">
                <div class="cube">

                    <span class="side top">
                        <a href="/motivasi" style="color:black;text-decoration:none">Motivation</a>
                    </span>
                    <span class="side front">Add</span>
                </div>
            </div>
        </div>
        <div class="col-sm-4 ">
            <div>
                <div class="uk-card uk-card-default uk-card-body">
                    @foreach($motivasi as $data)
                    <p style="font-family: 'Inconsolata', monospace;">{{$data->deskripsi}}</p>
                    <center>
                        <div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <a href="/update/{{$data->id}}">
                                        <img src="https://img.icons8.com/cotton/20/null/edit--v2.png"/></a>
                                </div>
                                <div class="col-sm-6">
                                    <form action="{{ route('home', $data->id) }}" method="POST">
                                        @csrf @method('DELETE')
                                        <button style="border:none"><img src="https://img.icons8.com/parakeet/20/null/delete.png"/></button>
                                    </form>

                                </div>
                            </div>

                        </div>
                    </center>
                    @endforeach
                </div>
            </div>
        </div>

    </div>
</div>
<br><br>
<div
    class="uk-child-width-1-3@m"
    uk-grid="uk-grid"
    uk-scrollspy="cls: uk-animation-fade; target: .uk-card; delay: 500; repeat: true">
    @foreach($catatan as $item)
    <div>
        <div class="uk-card uk-card-default uk-card-body">
            <h3 class="uk-card-title">{{$item->judul}}</h3>
            <p>
                <mark style="background: #ff9900">{{$item->sifat}}</mark>
            </p>
            <p>{{$item->deskripsi}}</p>
            <div>
                <center>
                    <div>
                        <div class="row">
                            <div class="col-sm-6">
                                <a href="/catatan/{{$item->id}}">
                                    <img src="https://img.icons8.com/cotton/20/null/edit--v2.png"/></a>
                            </div>
                            <div class="col-sm-6">
                                <form action="{{ route('home', $item->id) }}" method="POST">
                                    @csrf @method('DELETE')
                                    <button style="border:none"><img src="https://img.icons8.com/parakeet/20/null/delete.png"/></button>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
</div>
            @endforeach
  
@endsection