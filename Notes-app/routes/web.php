<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PageController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\CatatanController;
use App\Http\Controllers\MotivationController;

use App\Http\Controllers\RegisterController;
// RegisterController
// LoginController
// CatatanController

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [PageController::class, 'landing']);

Route::get('/home', [PageController::class, 'halaman']);


Route::get('/login',[LoginController::class, 'login']);

//create
Route::get('/create', [CatatanController::class, 'create']);
Route::post('/create', [CatatanController::class, 'store']);


//register user
Route::post('/register', [RegisterController::class, 'store']);
Route::get('/register',[RegisterController::class, 'register']);

//user register
Route::post('/login', [LoginController:: class, 'authenticate']);

//motivasi
Route::get('/motivasi', [PageController::class, 'motivasi']);
Route::post('/motivasi', [MotivationController::class, 'store']);
Route::post('/update', [MotivationController::class, 'edit']);
Route::get("/update/{id}", [MotivationController::class, 'edit']);

Route::post("/update/{id}", [MotivationController::class, 'update']);
Route::delete('/delete/{id}', [MotivationController::class,'destroy'])->name('home');
Route::delete('/delete/{id}', [CatatanController::class,'destroy'])->name('home');


Route::get("/catatan/{id}", [CatatanController::class, 'edit']);

Route::post("/catatan/{id}", [CatatanController::class, 'update']);

//search
Route::get('/search', [CatatanController::class,'search'])->name('search');

Route::post('/logout', [LoginController:: class, 'logout']);
