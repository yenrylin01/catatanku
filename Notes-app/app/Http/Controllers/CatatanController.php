<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Catatan;


class CatatanController extends Controller
{
    public function create(){
        return view('form.create');
    }
    public function store(Request $request){

        $request->validate([
            'judul' => 'required',
            'sifat' => 'required',
        'deskripsi' => 'required'
        ]);
        $catatan = new Catatan;
        $catatan->judul = $request->judul;
        $catatan->sifat = $request->sifat;
        $catatan->deskripsi = $request->deskripsi;

        $catatan->save();
        $catatan->id;
        return redirect('/home')->with('sukses','Company has been created successfully.');
        }
        public function update(Request $request, $id)
        {
            $catatan = Catatan::find($id);
           $catatan ->update($request->all());
           return redirect()->intended('home')->with("flash_message", "Motivasi berhasil Diubah");
        }
        public function edit($id){
            $catatan = Catatan::find($id);
            return view('form.update',compact('catatan'));
        }
        public function search(Request $request)
        {
            $keyword = $request->input('keyword');
            $results = Catatan::where('judul', 'like', '%'.$keyword.'%')->get();
            return view('home', ['results' => $results]);
    }
    



        public function destroy($id)
        {
            Catatan::destroy($id);
            return redirect()->intended('home');
        }
}
