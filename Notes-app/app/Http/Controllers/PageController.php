<?php

namespace App\Http\Controllers;
use App\Models\Motivasi;
use Illuminate\Support\Facades\DB;


use Illuminate\Http\Request;

class PageController extends Controller
{
    public function halaman(){
        DB::statement("SET SQL_MODE=''");
        $motivasi = DB::table('motivasi')->latest('id')->paginate(1);
        $catatan = DB::table('catatan')->latest('id')->get();
        return view('home')->with(['motivasi'=>$motivasi,'catatan'=>$catatan]);
        }
    
    public function landing(){
        return view('landing');
    }
    public function motivasi(){
        return view ('motivasi');
    }
}
