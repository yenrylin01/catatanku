<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Motivasi;
use Illuminate\Support\Facades\DB;

class MotivationController extends Controller
{
    public function store(Request $request){

$request->validate([
'deskripsi' => 'required'
]);
$motivasi = new Motivasi;
$motivasi->deskripsi = $request->deskripsi;
$motivasi->save();
return redirect('/home')->with('sukses','Company has been created successfully.');
}


    public function update(Request $request, $id)
    {
        $motivasi = Motivasi::find($id);
       $motivasi ->update($request->all());
       return redirect()->intended('home')->with("flash_message", "Motivasi berhasil Diubah");
    }
    public function edit($id){
        $motivasi = Motivasi::find($id);
        return view('update',compact('motivasi'));
    }

    public function destroy($id)
    {
        Motivasi::destroy($id);
        return redirect()->intended('home');
    }
    }
    

